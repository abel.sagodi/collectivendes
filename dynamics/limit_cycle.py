# Functions for the dynamics (with a limit cycle) for the Wilson-Cowan model
from typing import Sequence
import numpy as np


def sigmoid(x):
    """The logistic function"""
    return 1./(1+np.exp(-x))


def wilson_cowan_derivative(
    t, state,
    w: Sequence=(16.,16.,-12.,-4.),
    p: Sequence=(-1., -4.),
    r: float=0.,
):
    """2-population Wilson-Cowan dynamics with sigmoidal IR curve.
    
    `state`: Sequence of 2 firing rates for the two populations.
    `w`: Sequence of average synaptic gains, (w_{1->1}, w_{1->2}, w_{2->1}, w_{2->2})
    `p`: Sequence of 2 independent inputs to the two populations.
    `r`: float, normalised refactory period, normalised so that the time constant of the overall dynamics is 1.
    """
    state = np.array(state)
    w = np.array(w).reshape(2,2)
    p = np.array(p)
    non_refraction = 1-r*state
    # i->j syntax => left multiplying weights by states
    newly_activated_neurons = sigmoid(state@w + p)
    
    dstatedt = -state + non_refraction*newly_activated_neurons
    return dstatedt
