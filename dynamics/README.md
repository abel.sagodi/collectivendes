# dynamics

These python files can be used to generate samples of data from dynamical systems.

## different systems

* [lorenz.py](./lorenz.py) implements the [Lorenz dynamics](https://en.wikipedia.org/wiki/Lorenz_system). It can be used to asses fitting a chaotic system.
* [limit_cycle.py](./limit_cycle.py) implements the [Wilson-Cowan dynamics](https://en.wikipedia.org/wiki/Wilson%E2%80%93Cowan_model) for a 2-population system (by default) with a logistic sigmoid function. This system has a limit cycle (in part of its parameter space) which let's one consider fitting dynamics with an attracting manifold.
* [spp.py](./spp.py) implements a model of [self-propelled particles](https://en.wikipedia.org/wiki/Self-propelled_particles) with alignment, attraction, and avoidance (or repulsion) forces, and a location-dependent force to keep the particles within an arena. This can be used for developing models that fit/predict pairwise-rule-dependent collective behaviour.
* [stochastic.py](./stochastic.py) facilitates taking any of the previous models and using them as the drift function for an Itô process with a custom diffusion function, and simulating trajectories using the Euler-Maruyama integration scheme.

## conventions

The derivatives, if integrated with scipy.integrate.solve_ivp, will have the shape `(sampled_trajectories, *dimensionality_of_system, timepoints)`.