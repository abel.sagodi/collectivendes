# Functions for the Lorenz system derivative

def lorenz_derivative(t, state, rho=26., sigma=10., beta=8./3):
    """
    Computes the derivative of the Lorenz system.
    `state`: triplet of the x,y,z values of the system
    `t`: the time of the dynamics
    `rho`, `sigma`, `beta`: parameters
    
    Returns tuple of derivative values.
    """
    x,y,z = state
    return sigma*(y-x), x*(rho-z)-y, x*y-beta*z

