# Functions for self-propelled particle dynamics

import numpy as np
from scipy.stats import norm

# functions for boids model
def safe_normalise(x, extra_factor=0.0001):
    """Normalises the vectors with a little extra in the denominator 
    to avoid division by zero.
    """
    return x / (extra_factor + np.sqrt(np.sum(x**2)))
    

def euclidean_distance(x1,x2):
    """Too far, too close, just right... these distances need a metric."""
    return np.sqrt(np.sum((x1-x2)**2))

def f_approach(x_focal, x_neighbour):
    """If individuals get too far from each other,
    they will move together with this strength.
    """
    return norm.pdf(
        euclidean_distance(x_focal, x_neighbour),
        loc=4., scale=1.)

def f_avoid(x_focal, x_neighbour):
    """If individuals get too close to each other,
    they will move apart with this strength.
    """
    return norm.pdf(
        euclidean_distance(x_focal, x_neighbour),
        loc=0., scale=1.)

def f_align(x_focal, x_neighbour):
    """If individuals are the correct distance enough to each other,
    they will align with this strength.
    """
    return norm.pdf(
        euclidean_distance(x_focal, x_neighbour),
        loc=2., scale=1.)

def f_centralpull(x_focal):
    """This stops the individuals from running/flying/swimming away.
    
    Should be applied in the direction *antiparallel* to the positional vector. Note very realistic for fish.
    """
    return np.sum(x_focal**2)

def f_boundarypush(x_focal, R_arena=10.):
    r = np.sqrt(np.sum(x_focal**2))
    r_diff = R_arena - r
    strength = norm.pdf(r_diff, loc=4., scale=1.)-1.*norm.pdf(r_diff, loc=0., scale=.8)+norm.pdf(r_diff, loc=2., scale=1.)
    #TODO: include an orthogonal push along boundary instead of towards center
    if r > R_arena:
        strength -= 3*r
    return strength


def spp_derivative(t, state, boundary_push=True, normalised=False, *args, **kwargs):
    """Implements the derivative for the 4n-dimensional self-propelled
    particle dynamics. For each ofthe n individuals, there are 2 spatial
    and 2 velocity components.
    
    `t`: the time of the system.
    `state`: the 4n-dimensional vector or (4,n)-shaped matrix state of
    the system.
    `normalised`: bool, whether to normalise directional vectors.
    
    NOTE: *Normalised* directional vectors weren't used before.
    """
    # reshape the state into matrix if it isn't already
    positions_and_velocities = state.reshape(4,-1)
    num_individuals = positions_and_velocities.shape[1]
    derivatives = np.zeros_like(positions_and_velocities)
    # spatial derivatives *are* the velocity values
    derivatives[:2, :] = positions_and_velocities[2:4, :]
    
    
    for focal_idx in range(num_individuals):
        # Compute focal force due to location in arena
        # TODO: this should probably have a component 
        # parallel to the boundary of the arena as 
        # well as (this) component orthogonal to it.
        position_vector = positions_and_velocities[:2, focal_idx]
        if normalised:
            # position_vector /= np.sqrt(np.sum(position_vector**2))
            position_vector = safe_normalise(position_vector)
        
        if boundary_push:
            # TODO: make force orthogonal to the arena
            acceleration = f_boundarypush(
                positions_and_velocities[:2, focal_idx]
                )*position_vector
        else:
            # central pull (not so accurate for fish)
            acceleration = -f_centralpull(
                positions_and_velocities[:2, focal_idx]
                )*position_vector
        
        # Iterate over all the neighbours and sum their social forces
        for neighbour_idx in range(num_individuals):
            if focal_idx == neighbour_idx: continue
            
            direction_to_neighbour = positions_and_velocities[:2, neighbour_idx]\
                - positions_and_velocities[:2, focal_idx]
            if normalised:
                # direction_to_neighbour /= np.sqrt(
                #     np.sum(direction_to_neighbour**2))
                direction_to_neighbour = safe_normalise(direction_to_neighbour)
            
            neighbour_movement_direction = positions_and_velocities[2:4, neighbour_idx]
            if normalised:
                # neighbour_movement_direction /= np.sqrt(
                #     np.sum(neighbour_movement_direction**2))
                neighbour_movement_direction = safe_normalise(neighbour_movement_direction)
            
            acceleration += f_approach(
                positions_and_velocities[:2, focal_idx],
                positions_and_velocities[:2, neighbour_idx]
                )*direction_to_neighbour
            acceleration -= f_avoid(
                positions_and_velocities[:2, focal_idx],
                positions_and_velocities[:2, neighbour_idx]
                )*direction_to_neighbour
            acceleration += f_align(
                positions_and_velocities[:2, focal_idx],
                positions_and_velocities[:2, neighbour_idx]
                )*neighbour_movement_direction
        derivatives[2:, focal_idx] = acceleration
    return derivatives