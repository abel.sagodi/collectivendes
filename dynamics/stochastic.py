# File for producing stochastic differential equations from various dynamics
from typing import Callable, Any, Optional, Sequence
from scipy.linalg import cholesky
import numpy as np


def _get_covariance_root(
    brownian_covariance,
    covariance_root,
    state,
    covariance_is_diagonal
):
    """Helper function to compute the root Brownian covariance matrix."""
    if (brownian_covariance is None) and (covariance_root is None):
        covariance_root = np.eye(state.shape[0])
    if covariance_root is None:
        # if the covariance matrix is diagonal, we can skip the costly 
        # Cholesky decomposition
        if covariance_is_diagonal is None:
            covariance_is_diagonal = np.count_nonzero(brownian_covariance - np.diag(np.diagonal(brownian_covariance))) == 0

        if not covariance_is_diagonal:
            covariance_root = cholesky(brownian_covariance, lower=True)
        else:
            covariance_root = np.sqrt(brownian_covariance)
    return covariance_root


def stochastic_differential(
    t, state,
    drift_func: Callable[float, Any],
    diffusion_func: Callable[float, Any],
    delta_t: float=0.01,
    brownian_covariance: Optional=None,
    covariance_root: Optional=None,
    covariance_is_diagonal: Optional[bool]=None,
    return_brownian_motion: bool=False
):
    """Computes the stochastic differential for Itô process integration by Euler-Maruyama.
    
    `t`: time of evaluation.
    `state`: state of the process.
    `drift_func`: a function mapping (t,state) to the drift vector.
    `diffusion_func`: a function mapping (t,state) to the diffusion matrix.
    `brownian_covariance`: Optional, the covariance matrix of the underlying
        brownian motion.
    `covariance_root`: Optional, the square root of the covariance matrix.
        If neither `brownian_covariance` nor `covariance_root` is provided,
        it is assumed to be the identity matrix.
    `covariance_is_diagonal`: bool or None. If given, specifies whether covariance 
        matrix is diagonal (and if Cholesky decomposition can be skipped).
    `delta_t`: timestep of the integration scheme.
    `return_brownian_motion`: bool (False), whether to return the Brownian motion.
    """    
    state = np.array(state)
    drift = drift_func(t, state)
    diffusion_strength = diffusion_func(t, state)
    brownian_motion = np.random.randn(*state.shape)
    if (brownian_covariance is None) and (covariance_root is None):
        covariance_root = np.eye(state.shape[0])
    
    # It is generally more efficient for repeated function calls to 
    # provide the covariance root.
    covariance_root = _get_covariance_root(
        brownian_covariance,
        covariance_root,
        state,
        covariance_is_diagonal)
    
    # brownian motion is divided by square root of time as its variance
    # should grow with the square root of time
    brownian_motion = covariance_root @ brownian_motion / np.sqrt(delta_t)
    
    diffusion = diffusion_strength @ brownian_motion
    
    if return_brownian_motion:
        return drift + diffusion, brownian_motion
    return drift + diffusion
    
def sdeint_em(
    drift_func: Callable[float, Any],
    diffusion_func: Callable[float, Any],
    state0: Sequence, 
    t_eval: Sequence,
    brownian_covariance: Optional=None,
    covariance_root: Optional=None,
    covariance_is_diagonal: Optional[bool]=None
):
    """Uses the Euler-Maruyama integration scheme to solve the SDE.
    
    `drift_func`: a function mapping (t,state) to the drift vector.`
    `diffusion_func`: a function mapping (t,state) to the diffusion matrix.
    `state0`: initial state of the process.
    `t_eval`: times at which the drift and diffusion terms are evaluated.
    `brownian_covariance`: Optional, the covariance matrix of the underlying
        brownian motion.
    `covariance_root`: Optional, the square root of the covariance matrix.
        If neither `brownian_covariance` nor `covariance_root` is provided,
        it is assumed to be the identity matrix.
    `covariance_is_diagonal`: bool or None. If given, specifies whether covariance 
        matrix is diagonal (and if Cholesky decomposition can be skipped).
    """
    # compute covariance root if necessary
    state0 = np.array(state0)
    t_eval = np.array(t_eval)
    covariance_root = _get_covariance_root(
        brownian_covariance,
        covariance_root,
        state0,
        covariance_is_diagonal)
    
    # run integration scheme
    states = np.zeros((t_eval.shape[0], *state0.shape))
    states[0,...] = state0
    for i, dt in enumerate(np.diff(t_eval)):
        differential = stochastic_differential(
            t_eval[i], states[i,...],
            drift_func=drift_func,
            diffusion_func=diffusion_func,
            delta_t=dt,
            covariance_root=covariance_root
            )
        states[i+1,...] = states[i,...] + dt*differential
    return states