# A file for helper functions for the dynamics module
from typing import Sequence
import numpy as np
from scipy.stats.qmc import Sobol

def sobol_initial_values(
    state_dimensions: Sequence[int], 
    log2_samples: int=0,
):
    """Samples a Sobol sequence of initial values.
    
    `state_dimensions`: the shape of the state of the dynamics.
    `log2_samples`: the log2 of the number of samples. Sobol sequences 
        require a power of 2-many samples to be unbiased.
    
    Returns an array of shape `(2**log2_samples,*state_dimensions)`
    """
    full_dim = np.prod(state_dimensions)
    sampler = Sobol(full_dim)
    initial_values = sampler.random_base2(log2_samples)
    initial_values = initial_values.reshape(-1, *state_dimensions)
    return initial_values
    

def subsample_timeseries(
    samples, t_eval, step_size=1
):
    """Subsamples the timeseries. Maintains the timesteps between
    consecutive datapoints. This is so that the data remains regularly
    sampled and we can easily parallelise across batch dimension.
    """
    subsampled_samples = samples[...,::step_size]
    subsampled_t_eval = t_eval[...,::step_size]
    return subsampled_samples, subsampled_t_eval