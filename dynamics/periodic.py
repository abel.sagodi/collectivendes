import numpy as np

def harmonic_oscillator_derivative(
    t, state,
    zeta: float = 0.,
):
    """2-dimensional harmonic oscillator, potentially dampted. See
    https://en.wikipedia.org/wiki/Harmonic_oscillator#Universal_oscillator_equation
    
    In brief,
        ```
        loc, vel = state
        dvel_dt = -(2*zeta+q)
        dloc_dt = vel
        ```
    
    `t`: time of the system, included for consistency.
    `state`: state of the system. Can be a batch with batch dimension 0.
    `zeta`: multiplier for acceleration's dependence on velocity.
        The system is normalised so that the time constant is 1.
    """
    multiplier = np.array([
        [0, 1],  # derivative of location is velocity
        [-1, 0]  # undamped derivative of velocity is negative location
    ])
    dstatedt = state @ multiplier.T  # transposed to support batching
    # add damping
    dstatedt[...,1] += -2*zeta*state[...,1]
    return dstatedt