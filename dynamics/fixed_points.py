"""See https://github.com/xjwanglab/book/blob/master/wong2006/wong2006.py

The code has been adapted so that the model is autonomous, and only in the
'stimulus on' state. Hysteresis effects won't be observed as the stimulus
won't change.
"""

import numpy as np

# helper function
def F(I, a=270., b=108., d=0.154):
    return (a*I - b)/(1.-np.exp(-d*(a*I-b)))

def wong_wang_drift(
    t, state,
    coh         = 30.,   # coherence %
    gE          = 0.2609,
    gI          = -0.0497, # cross-inhibition strength [nA]
    I0          = 0.3255, # background current [nA]
    tauS        = 0.1, # Synaptic time constant [sec]
    gamma       = 0.641, # Saturation factor for gating variable
    tau0        = 0.002, # Noise time constant [sec]
    mu0         = 20., # Stimulus firing rate [Hz]
    Jext        = 0.52, # Stimulus input strength [pA/Hz]
    *args, **kwargs
):
    S1, S2 = state[:2]
    Ieta1, Ieta2 = state[2:]
    
    mean_stim = mu0 * Jext / 1000
    diff_stim = Jext * mu0 * coh/100. * 2
    
    Istim1 = mean_stim + diff_stim/2/1000
    Istim2 = mean_stim - diff_stim/2/1000
    
    Isyn1 = gE*S1 + gI*S2 + Istim1 + Ieta1
    Isyn2 = gE*S2 + gI*S1 + Istim2 + Ieta2
    
    r1 = F(Isyn1)
    r2 = F(Isyn2)
    
    dS1dt = -S1/tauS + (1.-S1)*gamma*r1
    dS2dt = -S2/tauS + (1.-S2)*gamma*r2
    Ieta1_drift = (1./tau0)*(I0-Ieta1)
    Ieta2_drift = (1./tau0)*(I0-Ieta2)
    
    return np.array([dS1dt, dS2dt, Ieta1_drift, Ieta2_drift])


def wong_wang_diffusion(
    t, state,
    tau0        = 0.002, # Noise time constant [sec]
    sigma       = 0.02, # Noise magnitude [nA]
    *args, **kwargs
):
    Ieta_diffusion = sigma*np.sqrt(1/tau0)
    return np.diag([0., 0., Ieta_diffusion, Ieta_diffusion])


if __name__ == '__main__':
    from stochastic import sdeint_em
    
    t_eval = np.linspace(0., 2., 1000)
    S1, S2 = np.random.rand(2)*0.2
    state0 = np.array([S1, S2, 0., 0.])
    
    print("Attempting Euler-Maruyama integration")
    trajectory = sdeint_em(
        drift_func=wong_wang_drift,
        diffusion_func=wong_wang_diffusion,
        state0=state0,
        t_eval=t_eval)
    print("Success?")
    print(trajectory)
    