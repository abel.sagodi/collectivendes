# Scripts

Scripts in the folder can be used to run from the commandline the fitting of neural networks described in [models](../models) on the dynamical systems described in [dynamics](../dynamics).

An example of training several models in parallel from bash can be found in [`train_multiple.sh`](./train_multiple.sh).

[`make_mp4s.py`](./make_mp4s.py) uses `imageio` to create mp4 videos from the sequences of saved images (saved by [`train_plot.py`](./train_plot.py)). It is used as
```
python -m make_mp4s experiment/folder
```
The mp4s will be saved in the designated experiment folder.
