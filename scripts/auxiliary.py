import os, sys
from typing import Optional

# hack to be able to import from elsewhere:
# add parent directory of scripts folder to path
# to be able to import from neighbouring submodules
# !!!!argh python WWWHHYYY!!!!
current_dir = os.path.dirname(__file__)
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)

import torch
from torch import optim
from torchdiffeq import odeint
from models.neuralnets import MLP_NeuralODE, Pairwise_NeuralODE
from models.tools import _cycle_dimensions

device = 'cuda' if torch.cuda.is_available() else 'cpu'

MODELS_DICT = dict(
    mlp=MLP_NeuralODE,
    pairwise=Pairwise_NeuralODE)

def create_network(
    hidden_layers,
    state_dimensions: Optional=None,
    model_type: str='mlp',
    device: str=device,
    load_model: Optional[str]=None,
    num_ind: Optional[int]=None,
    dynamics: Optional[str]=None,
    *args, **kwargs
):
    if state_dimensions is None:
        print("Attempting to infer state dimensions.")
        if dynamics == 'spp':
            if model_type == 'mlp' and num_ind is None:
                raise Exception(
                    "Model type mlp does not support unknown num_ind")
            elif num_ind is None:
                num_ind = 0
            state_dimensions = (4,num_ind)
        elif dynamics == 'wc':
            state_dimensions = (2,)
        elif dynamics == 'lorenz':
            state_dimensions = (3,)
        else:
            raise Exception(
                "Unable to infer state dimensions.")
    neuralde = MODELS_DICT[model_type](
        state_dimensions=state_dimensions,
        hidden_layers=hidden_layers,
        device=device)
    if load_model:
        print("Loading pretrained model...")
        neuralde.load_state_dict(torch.load(load_model))
    return neuralde

def simulate_trajectories_to_numpy(
    neuralde,
    initial_values,
    t_eval,
    device=device
):
    with torch.no_grad():
        estimated_trajectories = odeint(
            neuralde,
            t=torch.from_numpy(t_eval).to(device),
            y0=torch.from_numpy(
                initial_values
                # data[sample_indices,...,0]
                ).to(device))
        estimated_trajectories = _cycle_dimensions(
            estimated_trajectories).cpu().numpy()
    return estimated_trajectories
