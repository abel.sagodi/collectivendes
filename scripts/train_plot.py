import os, sys
from typing import Optional

# hack to be able to import from elsewhere:
# add parent directory of scripts folder to path
# to be able to import from neighbouring submodules
# !!!!argh python WWWHHYYY!!!!
current_dir = os.path.dirname(__file__)
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp

from dynamics.spp import spp_derivative
from dynamics.limit_cycle import wilson_cowan_derivative
from dynamics.lorenz import lorenz_derivative
from dynamics.periodic import harmonic_oscillator_derivative
from dynamics.fixed_points import wong_wang_drift, wong_wang_diffusion
from dynamics.stochastic import sdeint_em
from dynamics.tools import sobol_initial_values

import torch
# import both, choose between them later
from torchdiffeq import odeint, odeint_adjoint

from models.tools import get_batch, train_model, _cycle_dimensions,\
    PlottingCallback, EarlyStoppingCallback, SaveModelCallback
from models.neuralnets import MLP_NeuralODE, Pairwise_NeuralODE
from auxiliary import create_network, simulate_trajectories_to_numpy

# to run the script, use argparse
import argparse
from datetime import datetime
import json
from functools import partial

default_device = 'cuda' if torch.cuda.is_available() else 'cpu'

parser = argparse.ArgumentParser(
    description="""
    A script to run training of the neuralode models on various dynamics.
    """)
parser.add_argument(
    "--savedir",
    type=str,
    default="experiments",
    help="""
    Folder to save plots in. Individual experiments have a prefix folder of 
    the datetime.
    """)
parser.add_argument(
    "--device",
    type=str,
    default=default_device,
    help="Device to use for torch: 'cuda' or 'cpu'.")
parser.add_argument(
    "--tqdm",
    action='store_true',
    help="Whether to use tqdm to print progress.")

# neuralde args
neuralde_keys = ['mlp', 'pairwise']
parser.add_argument(
    "--model_type",
    type=str, default=neuralde_keys[0],
    help="Type of model for neuralde.")
parser.add_argument(
    "--hidden_layers",
    type=int, nargs="+",
    default=[64],
    help="Sizes of hidden layers.")
parser.add_argument(
    "--load_model",
    type=str,
    default=None,
    help="If given, where to load the pretrained model.")

# learning parameters
optimizer_keys = ['rmsprop', 'sgd', 'adam', 'adadelta']
parser.add_argument(
    "--optim", type=str,
    default=optimizer_keys[0],
    help="Optimizer for fitting parameters.")
parser.add_argument(
    "--lr", type=float,
    default=1e-3,
    help="Learning rate for optimizer. No other parameters are exposed.")
parser.add_argument(
    "--training_steps", type=int,
    default=2000,
    help="Number of training steps.")
parser.add_argument(
    "--early_stopping_steps", type=int,
    default=200,
    help="Number of training steps without improvement before early stopping.")
parser.add_argument(
    "--batch_size", type=int,
    default=256,
    help="Batch size for training.")
parser.add_argument(
    "--batch_timesteps", type=int,
    default=200,
    help="Length in timesteps of batch trajectories.")
parser.add_argument(
    "--callback_every", type=int,
    default=50,
    help="Frequency by which to run callbacks.")
parser.add_argument(
    "--train_split", type=float,
    default=0.8,
    help="Fraction (up to rounding) of sampled trajectories used for training.")

# general dynamics arguments
dynamics_keys = ['spp', 'wc', 'lorenz', 'ho', 'ww']
dynamics_descriptions = [
    'self-propelled particles',
    'wilson cowan',
    'lorenz',
    'harmonic oscillator',
    'wong wang']
parser.add_argument(
    "--dynamics",
    type=str,
    default=dynamics_keys[0],
    help="Which dynamical system to fit."
        +f"\nOptions are: "
        +", ".join(list('='.join(x) for x in zip(
            dynamics_keys, dynamics_descriptions))))
parser.add_argument(
    "--log2_samples",
    type=int,
    default=0,
    help="Base-2 logarithm of number of sampled trajectories to fit.")
parser.add_argument(
    "--tmin", type=float,
    default=0., help="Starting time of integration.")
parser.add_argument(
    "--tmax", type=float,
    default=10., help="Ending time of integration.")
parser.add_argument(
    "--timesteps", type=int,
    default=1000, help="Ending time of integration.")
parser.add_argument(
    "--brownian_var", type=float,
    default=0., help="Variance of diagonal brownian motion.")

# SPP arguments
parser.add_argument(
    "--spp_boundary_push",
    action='store_true')
parser.add_argument(
    "--spp_unnormalised",
    action='store_true')
parser.add_argument(
    "--num_ind",
    type=int, default=2,
    help="Number of individual SPPs.")
parser.add_argument(
    "--num_sample_plots",
    type=int, default=2,
    help="Number of plots of dynamics of SPPs.")

args = parser.parse_args()


# dictionaries which don't depend on arguments
OPTIMIZERS_DICT = dict(
    rmsprop=torch.optim.RMSprop,
    sgd=torch.optim.SGD,
    adam=torch.optim.Adam,
    adadelta=torch.optim.Adadelta)


def setup_experiment(
    params_dict=args.__dict__
):
    """Sets up the experiment, and returns the parameters for training a model.
    The purpose of this is to allow repeat experiments by loading the params of
    a previous experiment.
    
    `params_dict`: The list of parameters for running the experiment.
        It is updated by `args.__dict__` where keys are missing.
    """
    # import args values to extend params dict
    for k in args.__dict__:
        if k not in params_dict:
            params_dict[k] = args.__dict__[k]
    
    # general constants
    device = params_dict['device']
    if params_dict['tqdm']:
        from tqdm import tqdm
    else:
        tqdm = lambda x: x
    
    # set-up experiment
    # convert script arguments into parameters
    # first some helpful maps between argss and functions
    DYNAMICS_DICT = dict(
        spp=lambda t,state: spp_derivative(
            t, state, 
            boundary_push=params_dict['spp_boundary_push'],
            normalised=not params_dict['spp_unnormalised'],
            ).reshape(-1),
        wc=wilson_cowan_derivative,
        lorenz=lorenz_derivative,
        ho=harmonic_oscillator_derivative,
        ww=wong_wang_drift)
    DIFFUSIONS_DICT = dict(
        spp=lambda t,state: np.diag(np.ones_like(state)),
        wc=lambda t,state: np.diag(state*(1.-state)),
        lorenz=lambda t,state: np.diag(np.ones_like(state)),
        ho=lambda t,state: np.diag(np.ones_like(state)),
        ww=wong_wang_diffusion)
    DIMENSIONS_DICT = dict(
        spp=(4,params_dict['num_ind']),
        wc=(2,),
        lorenz=(3,),
        ho=(2,),
        ww=(4,))
    
    running_time = datetime.now().isoformat().replace(':','_')
    experiment_foldername = '_'.join([params_dict['dynamics'], running_time])

    all_experiments_dir = os.path.join(current_dir, params_dict['savedir'])
    if not os.path.exists(all_experiments_dir):
        os.mkdir(all_experiments_dir)
    experiment_dir = os.path.join(all_experiments_dir,experiment_foldername)

    if os.path.exists(experiment_dir):
        # experiment_dir is not precise enough
        experiment_dir = experiment_dir + f"_rand{np.random.randint(10000)}"
    os.mkdir(experiment_dir)

    parameters_fname = os.path.join(experiment_dir, 'parameters.json')
    print("Saving parameters...")
    with open(parameters_fname, 'w') as params_file:
        json.dump(params_dict, params_file)

    dynamics_plot_fname_prefix = os.path.join(experiment_dir, 'dynamics')
    vectorfield_plot_fname_prefix = os.path.join(experiment_dir, 'vectorfied')
    loss_plot_fname_prefix = os.path.join(experiment_dir, 'losses')
    model_fname_prefix = os.path.join(experiment_dir, 'model')

    deriv_func = DYNAMICS_DICT[params_dict['dynamics']]
    diffusion_func = DIFFUSIONS_DICT[params_dict['dynamics']]
    state_dimensions = DIMENSIONS_DICT[params_dict['dynamics']]
    t_span = (params_dict['tmin'], params_dict['tmax'])
    t_eval = np.linspace(t_span[0], t_span[1], params_dict['timesteps'])
    initial_values = sobol_initial_values(
        state_dimensions, params_dict['log2_samples'])
    if params_dict['dynamics'] == 'lorenz':
        initial_values *= np.array((40.,40.,50.))[np.newaxis,...].repeat(
            initial_values.shape[0], axis=0)  # repeat for each batch
        initial_values += np.array((-20.,-20.,0.))[np.newaxis,...].repeat(
            initial_values.shape[0], axis=0)
    sampled_trajectories = np.empty(
        (2**params_dict['log2_samples'],
         *state_dimensions,
         params_dict['timesteps']))

    if params_dict['brownian_var'] > 0.:
        full_state_dimensionality = np.prod(state_dimensions)
        brownian_covariance = np.eye(
            full_state_dimensionality
            )*params_dict['brownian_var']

    print("Sampling trajectories...")
    if params_dict['brownian_var'] > 0.:
        print("\tTrajectories are with diffusive noise.")
    else:
        print("\tTrajectories are without noise.")
    for sample_idx in tqdm(range(sampled_trajectories.shape[0])):
        initial_value = initial_values[sample_idx,...].reshape(-1)
        if params_dict['brownian_var'] > 0.:
            # print(f"Sampling noisy trajectory {sample_idx}...")
            sample_traj = sdeint_em(
                drift_func=deriv_func,
                diffusion_func=diffusion_func,
                state0=initial_value,
                t_eval=t_eval,
                brownian_covariance=brownian_covariance,
                covariance_is_diagonal=True).T
        else:
            # print(f"Sampling noise-free trajectory {sample_idx}...")
            sample_traj = solve_ivp(
                deriv_func,
                t_span=t_span,
                y0=initial_value,
                t_eval=t_eval).y
        sampled_trajectories[sample_idx,...] = sample_traj.reshape(
            *state_dimensions, params_dict['timesteps'])

    num_training_samples = np.ceil(
        sampled_trajectories.shape[0]*params_dict['train_split']
        ).astype(int)
    if num_training_samples == sampled_trajectories.shape[0]:
        training_data = sampled_trajectories
        validation_data = None
        print(f"\tNot enough samples for {params_dict['train_split']:.2f} "
              + "train-val split.")
    else:
        indices_permuted = np.arange(sampled_trajectories.shape[0])
        training_indices = indices_permuted[:num_training_samples]
        validation_indices = indices_permuted[num_training_samples:]
        training_data = sampled_trajectories[training_indices,...]
        validation_data = sampled_trajectories[validation_indices,...]
        print(f"\t{num_training_samples} of {sampled_trajectories.shape[0]} "
              + "trajectories allocated to training.")
    print("Saving data...")
    training_data_fname = os.path.join(experiment_dir, 'training_data.npy')
    validation_data_fname = os.path.join(experiment_dir, 'validation_data.npy')
    np.save(training_data_fname, training_data)
    if validation_data is not None:
        np.save(validation_data_fname, validation_data)

    print(f"Creating {params_dict['model_type']} neuralde model...")
    neuralde = create_network(
        state_dimensions=state_dimensions,
        **params_dict)

    optimizer = OPTIMIZERS_DICT[params_dict['optim']](
        neuralde.parameters(), lr=params_dict['lr'])
    
    # create callbacks
    def _plot_losses(
        neuralde,
        losses,
        validation_losses
    ):
        fig, axes = plt.subplots(figsize=(5,5))
        axes.plot(losses, label='training losses')
        if validation_losses:
            axes.plot(validation_losses, label='validation losses')
        axes.legend()
        axes.set_title("Losses")
        iteration = len(losses)
        fig.suptitle(f"Iteration {iteration}")
        fig.tight_layout()
        filename = loss_plot_fname_prefix + f"_iter{iteration}.png"
        fig.savefig(filename)
        plt.close(fig)
    
    
    def _plot_dynamics_spp(
        neuralde,
        losses=None,
        validation_losses=None,
        data=None,
        t_eval=None,
        sample_indices=[0]
    ):
        # sample trajectory estimates
        estimated_trajectories = simulate_trajectories_to_numpy(
            neuralde=neuralde,
            t_eval=t_eval,
            initial_values=data[sample_indices,...,0],
            device=device)
        fig, axes = plt.subplots(
            1, len(sample_indices),
            figsize=(5*len(sample_indices), 5) )
        if not isinstance(axes, np.ndarray):
            axes = [axes]
        for est_idx, (sample_idx, ax) in enumerate(zip(sample_indices, axes)):
            for focal_idx in range(data.shape[2]):
                # ground truth
                ax.plot(
                    data[sample_idx, 0, focal_idx, :],
                    data[sample_idx, 1, focal_idx, :],
                    color="C"+str(focal_idx),
                    label='data' if focal_idx == 0 else None)
                ax.plot(
                    estimated_trajectories[est_idx, 0, focal_idx, :],
                    estimated_trajectories[est_idx, 1, focal_idx, :],
                    color="C"+str(focal_idx),
                    ls='--',
                    label='model' if focal_idx == 0 else None)
                ax.set_title(f"Trajectory {sample_idx}")
            ax.legend()
        iteration = len(losses)
        fig.suptitle(f"Iteration {iteration}")
        fig.tight_layout()
        filename = dynamics_plot_fname_prefix + f"_iter{iteration}.png"
        fig.savefig(filename)
        plt.close(fig)
        
    def _plot_dynamics_wc(
        neuralde,
        losses=None,
        validation_losses=None,
        data=None,
        t_eval=None,
        sample_indices=[0]
    ):
        estimated_trajectories = simulate_trajectories_to_numpy(
            neuralde=neuralde,
            t_eval=t_eval,
            initial_values=data[sample_indices,...,0],
            device=device)
        fig, ax = plt.subplots(figsize=(5, 5))
        for est_idx, sample_idx in enumerate(sample_indices):
            ax.plot(
                data[sample_idx,0,:],
                data[sample_idx,1,:],
                alpha=0.5, lw=4, color="C"+str(est_idx),
                label='ground truth' if est_idx==0 else None)
            ax.plot(
                estimated_trajectories[est_idx,0,:],
                estimated_trajectories[est_idx,1,:],
                ls='--', color="C"+str(est_idx),
                label='model prediction' if est_idx==0 else None)
        ax.vlines(
            [0., 1.], [0., 0.], [1., 1.], ls='-', color='black',
            label='feasible region')
        ax.hlines(
            [0., 1.], [0., 0.], [1., 1.], ls='-', color='black')
        ax.legend()
        ax.set_xlim(-0.1, 1.1)
        ax.set_ylim(-0.1, 1.1)
        iteration = len(losses)
        fig.suptitle(f"Iteration {iteration}")
        fig.tight_layout()
        filename = dynamics_plot_fname_prefix + f"_iter{iteration}.png"
        fig.savefig(filename)
        plt.close(fig)
        
    def _plot_dynamics_lorenz(
        neuralde,
        losses=None,
        validation_losses=None,
        data=None,
        t_eval=None,
        sample_indices=[0]
    ):
        estimated_trajectories = simulate_trajectories_to_numpy(
            neuralde=neuralde,
            t_eval=t_eval,
            initial_values=data[sample_indices,...,0],
            device=device)
        fig = plt.figure(figsize=(6, 6))
        ax = plt.axes(projection='3d')
        fig.add_axes(ax)
        for est_idx, sample_idx in enumerate(sample_indices):
            ax.plot(
                data[sample_idx,0,:],
                data[sample_idx,1,:],
                data[sample_idx,2,:],
                alpha=0.5, lw=4, color="C"+str(est_idx),
                label='ground truth' if est_idx==0 else None)
            ax.plot(
                estimated_trajectories[est_idx,0,:],
                estimated_trajectories[est_idx,1,:],
                estimated_trajectories[est_idx,2,:],
                ls='--', color="C"+str(est_idx),
                label='model prediction' if est_idx==0 else None)
        ax.legend()
        # limit the plot range for viewing pleasure
        ax.set_xlim(-20, 20)
        ax.set_ylim(-20, 20)
        ax.set_zlim(0, 50)
        iteration = len(losses)
        fig.suptitle(f"Iteration {iteration}")
        fig.tight_layout()
        filename = dynamics_plot_fname_prefix + f"_iter{iteration}.png"
        fig.savefig(filename)
        plt.close(fig)

    DYNAMICS_PLOT_DICT = dict(
        spp=partial(
            _plot_dynamics_spp,
            data=training_data,
            t_eval=t_eval,
            sample_indices=np.arange(params_dict['num_sample_plots'])),
        wc=partial(
            _plot_dynamics_wc,
            data=training_data,
            t_eval=t_eval,
            sample_indices=np.arange(params_dict['num_sample_plots'])),
        lorenz=partial(
            _plot_dynamics_lorenz,
            data=training_data,
            t_eval=t_eval,
            sample_indices=np.arange(params_dict['num_sample_plots'])),
        ho=None,
        ww=None)
    
#     def _plot_vectorfield_wilsoncowan(
#         neuralde,
#         losses=None,
#         validation_losses=None,
#         data=None,
#         t_eval=None,
#         sample_indices=[0]
#     ):
#         from scipy.stats import gaussian_kde  # needed for second plot
        
#         # wilson-cowan is bounded between 0 and 1
#         xy_points = np.linspace(-0.1, 1.1, 20)
        
#         # compute the differentials
#         X, Y = np.meshgrid(xy_points,xy_points)
#         dX, dY = np.empty_like(X), np.empty_like(Y)
#         dX_est, dY_est = np.empty_like(X), np.empty_like(Y)

#         # X is constant along columns
#         for col_idx, x in enumerate(X[0,:]):
#             # Y is constant along rows
#             for row_idx, y in enumerate(Y[:,0]):
#                 deriv = wilson_cowan_derivative(0, (x,y))
#                 dX[row_idx, col_idx] = deriv[0]
#                 dY[row_idx, col_idx] = deriv[1]
#                 with torch.no_grad():
#                     pred_deriv = neuralde(0., torch.tensor((x,y), device=device))
#                     pred_deriv = pred_deriv.cpu().numpy().squeeze()
#                     dX_est[row_idx, col_idx] = pred_deriv[0]
#                     dY_est[row_idx, col_idx] = pred_deriv[1]
        
#         fig, axes = plt.subplots(
#             1,2, figsize=(12,5),
#             sharex=True, sharey=True)
#         axes[0].set_xlim(-0.1, 1.1)
#         axes[0].set_ylim(-0.1, 1.1)
#         axes[0].quiver(X,Y,dX,dY)
#         axes[1].quiver(X,Y,dX_est,dY_est)

#         for i in range(len(axes)):
#             axes[i].vlines([0., 1.], [0., 0.], [1., 1.], ls='--', color='black')
#             axes[i].hlines([0., 1.], [0., 0.], [1., 1.], ls='--', color='black')

#         axes[0].set_title("Ground Truth Drift")
#         axes[1].set_title("Expected Differentials")
        
#         iteration = len(losses)
#         fig.suptitle(f"Iteration {iteration}")
#         fig.tight_layout()
#         filename = vectorfield_plot_fname_prefix + f"_vectorfields_iter{iteration}.png"
#         fig.savefig(filename)
#         plt.close(fig)
        
#         # second plot
#         # concatenate across all batches by appending to end arrays together
#         kernel = gaussian_kde(
#             np.concatenate(data, axis=-1))
#         fig, ax = plt.subplots(figsize=(5,5))
#         # higher density for density plot
#         xy_points_fine = np.linspace(-0.1, 1.1, 40)
#         X_fine, Y_fine = np.meshgrid(xy_points,xy_points)
#         positions = np.vstack([X_fine.ravel(),Y_fine.ravel()])
#         density = np.reshape(kernel(positions).T, X_fine.shape)
        
#         ax.contourf(X_fine, Y_fine, density, cmap='inferno', alpha=0.5)
#         ax.quiver(X,Y, dX_est-dX, dY_est-dY)
#         ax.vlines([0., 1.], [0., 0.], [1., 1.], ls='--', color='black')
#         ax.hlines([0., 1.], [0., 0.], [1., 1.], ls='--', color='black')
#         ax.set_title("Difference of True Drift From Estimated Derivative")
#         ax.set_xlim(-0.1, 1.1)
#         ax.set_ylim(-0.1, 1.1)
#         fig.suptitle(f"Iteration {iteration}")
#         fig.tight_layout()
#         filename = vectorfield_plot_fname_prefix + f"_difference_iter{iteration}.png"
#         fig.savefig(filename)
#         plt.close(fig)
        
    def _plot_vectorfield_2d(
        neuralde,
        losses=None,
        validation_losses=None,
        data=None,
        t_eval=None,
        sample_indices=[0],
        axes_min=-0.1,
        axes_max=1.1,
        state_suffix=None,
    ):
        """
        `state_suffix`: extra values to append to later dimensions for making the plot,
            such as mean noise levels for the Wong-Wang model.
        """
        from scipy.stats import gaussian_kde  # needed for second plot
        
        # wilson-cowan is bounded between 0 and 1
        xy_points = np.linspace(axes_min, axes_max, 20)
        
        # compute the differentials
        X, Y = np.meshgrid(xy_points,xy_points)
        dX, dY = np.empty_like(X), np.empty_like(Y)
        dX_est, dY_est = np.empty_like(X), np.empty_like(Y)

        # X is constant along columns
        for col_idx, x in enumerate(X[0,:]):
            # Y is constant along rows
            for row_idx, y in enumerate(Y[:,0]):
                if state_suffix is not None:
                    point = (x,y,*state_suffix)
                else:
                    point = (x,y)
                deriv = deriv_func(0, point)
                dX[row_idx, col_idx] = deriv[0]
                dY[row_idx, col_idx] = deriv[1]
                with torch.no_grad():
                    pred_deriv = neuralde(0., torch.tensor(point, device=device))
                    pred_deriv = pred_deriv.cpu().numpy().squeeze()
                    dX_est[row_idx, col_idx] = pred_deriv[0]
                    dY_est[row_idx, col_idx] = pred_deriv[1]
        
        fig, axes = plt.subplots(
            1,2, figsize=(12,5),
            sharex=True, sharey=True)
        axes[0].set_xlim(axes_min, axes_max)
        axes[0].set_ylim(axes_min, axes_max)
        axes[0].quiver(X,Y,dX,dY)
        axes[1].quiver(X,Y,dX_est,dY_est)

        # for i in range(len(axes)):
        #     axes[i].vlines([0., 1.], [0., 0.], [1., 1.], ls='--', color='black')
        #     axes[i].hlines([0., 1.], [0., 0.], [1., 1.], ls='--', color='black')

        axes[0].set_title("Ground Truth Drift")
        axes[1].set_title("Expected Differentials")
        
        iteration = len(losses)
        fig.suptitle(f"Iteration {iteration}")
        fig.tight_layout()
        filename = vectorfield_plot_fname_prefix + f"_vectorfields_iter{iteration}.png"
        fig.savefig(filename)
        plt.close(fig)
        
        # second plot
        # concatenate across all batches by appending to end arrays together
        if state_suffix is not None:
            # assume (data.shape) = (batch,dimensions,timepoints)
            data = data[:,:2,...]
        
        kernel = gaussian_kde(
            np.concatenate(data, axis=-1))
        fig, ax = plt.subplots(figsize=(5,5))
        # higher density for density plot
        xy_points_fine = np.linspace(axes_min, axes_max, 40)
        X_fine, Y_fine = np.meshgrid(xy_points,xy_points)
        positions = np.vstack([X_fine.ravel(),Y_fine.ravel()])
        density = np.reshape(kernel(positions).T, X_fine.shape)
        
        ax.contourf(X_fine, Y_fine, density, cmap='inferno', alpha=0.5)
        ax.quiver(X,Y, dX_est-dX, dY_est-dY)
        # ax.vlines([0., 1.], [0., 0.], [1., 1.], ls='--', color='black')
        # ax.hlines([0., 1.], [0., 0.], [1., 1.], ls='--', color='black')
        ax.set_title("Difference of True Drift From Estimated Derivative")
        ax.set_xlim(axes_min, axes_max)
        ax.set_ylim(axes_min, axes_max)
        fig.suptitle(f"Iteration {iteration}")
        fig.tight_layout()
        filename = vectorfield_plot_fname_prefix + f"_difference_iter{iteration}.png"
        fig.savefig(filename)
        plt.close(fig)
    
    
    VECTORFIELDS_PLOT_DICT = dict(
        spp=None,
        wc=partial(
            _plot_vectorfield_2d,
            data=training_data,
            t_eval=t_eval,
            sample_indices=np.arange(params_dict['num_sample_plots'])),
        lorenz=None,
        ho=partial(
            _plot_vectorfield_2d,
            data=training_data,
            t_eval=t_eval,
            axes_min=-1.1,
            axes_max=1.1,
            sample_indices=np.arange(params_dict['num_sample_plots'])),
        ww=partial(
            _plot_vectorfield_2d,
            data=training_data,
            t_eval=t_eval,
            sample_indices=np.arange(params_dict['num_sample_plots']),
            state_suffix=(0.3255,0.3255))  # wong-wang model is actually 4-dimensional
    )

    print("Setting up training...")
    callbacks = []
    dynamics_plot_func = DYNAMICS_PLOT_DICT[params_dict['dynamics']]
    if dynamics_plot_func is not None:
        dynamics_callback = PlottingCallback(
            model_reference=neuralde,
            plot_function=dynamics_plot_func,
            plot_every_n=params_dict['callback_every'])
        callbacks.append(dynamics_callback)
    vectorfields_plot_func = VECTORFIELDS_PLOT_DICT[params_dict['dynamics']]
    if vectorfields_plot_func is not None:
        vectorfields_callback = PlottingCallback(
            model_reference=neuralde,
            plot_function=vectorfields_plot_func,
            plot_every_n=params_dict['callback_every'])
        callbacks.append(vectorfields_callback)
    
    losses_callback = PlottingCallback(
        model_reference=neuralde,
        plot_function=_plot_losses,
        plot_every_n=params_dict['callback_every'])
    callbacks.append(losses_callback)

    if validation_data is not None:
        early_stopping_callback = EarlyStoppingCallback(
            model_reference=neuralde,
            steps_before_breaking=params_dict['early_stopping_steps'],
            load_best_on_breaking=True)
        callbacks.append(early_stopping_callback)

    save_callback = SaveModelCallback(
        model_reference=neuralde,
        save_fname_prefix=model_fname_prefix,
        save_every_n=params_dict['callback_every'])
    callbacks.append(save_callback)
    return dict(
        model=neuralde,
        optimizer=optimizer,
        training_data=training_data,
        training_t_eval=t_eval,
        validation_data=validation_data,
        validation_t_eval=t_eval,
        training_steps=params_dict['training_steps'],
        batch_size=params_dict['batch_size'],
        batch_timesteps=params_dict['batch_timesteps'],
        callbacks=callbacks,
        tqdm=tqdm)


if __name__ == '__main__':
    training_kwargs = setup_experiment(args.__dict__)
    print("Beginning training...")
    model, optimizer, losses, validation_losses = train_model(
        **training_kwargs)
    print("Done!")