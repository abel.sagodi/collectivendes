# CollectiveNDEs

A resource to explore the potential application of neural differential equations (NDEs, mostly NODEs) to predicting collective behavior. 


## Demonstration of the power of NDEs

First of all, to show that even chaotic orbits can be learned by NDEs, here is the process of the behavior of the fitted model over the process of learning:
<p align="center">
  <img width="600" height="450" src="./assets/dynamics_lorenz_learning.gif">
</p>


NDEs can be used to fit self-propelled particel model behavior:
<p align="center">
  <img width="600" height="450" src="./assets/spp5_1024_full_prediction_shift0.gif">
</p>


RNNs can be used to fit animal behavior (60 zebrafish, Danio rerio [[1]](#1)):
<p align="center">
  <img width="600" height="450" src="./assets/full_prediction_halfway.gif">
</p>
The predition of the model for one second is plotted against the actual motion of the animals.





## References
<a id="1">[1]</a> 
Heras, Francisco JH and Romero-Ferrero, Francisco and Hinz, Robert C and de Polavieja, Gonzalo G (2019)
Deep attention networks reveal the rules of collective motion in zebrafish
PLoS computational biology
