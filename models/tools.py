# tools for fitting neuralodes
# A note on dimensions:
# * Pytorch naturally broadcasts over the first dimension as a batch dimension, so I've maintained the first dimensions as the batch dimension.
# * The final dimension being time aligns with scipy's solve_ivp https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.solve_ivp.html which is agnostic of neural network framework.
# * This however raises a small problem: torchdiffeq returns the time dimension as the first dimension. As such, these returned torch.Tensors are permuted.
import torch
from typing import Sequence, Optional, Callable, Any
from torch.nn.functional import mse_loss

device = 'cuda' if torch.cuda.is_available() else 'cpu'

##region Data Handling
def get_batch(
    data, eval_times,
    batch_size=16, batch_timesteps=10,
    burn_in=0,
    device=device,
    dtype=torch.double
):
    """
    `data`: array of (samples, state-dimensions, timepoints) data.
    `eval_times`: sequence of timepoints, or array of 
        (samples, timepoints), of data sampling times.
    `batch_size`: number of batch subsamples to take.
    `batch_timesteps`: length in time of subsamples.
    `burn_in`: offset initial time to ignore to allow data to converge.
        
    Returns batch time points of shape (`batch_size`,batch_timesteps)
        and batch array of shape (`batch_size`,state-dimensions,batch_timesteps)
        
    Note: torchdiffeq.odeint assumes a 1-D array of timepoints for evaluation.
    Thus, currently, the system either needs to be autonomous or each batch sample
    needs to be integrated independently by the neuralode.
    """
    data = torch.tensor(data, device=device, dtype=dtype)
    eval_times = torch.tensor(eval_times, device=device, dtype=dtype)
    sample_indices = torch.randint(
        data.shape[0], size=(batch_size,), device=device)
    start_time_indices = torch.randint(
        data.shape[-1]-batch_timesteps-burn_in,
        size=(batch_size,), device=device)+burn_in
    if len(eval_times.shape) == 1:
        # repeat evaluation times to create an array
        eval_times = eval_times.repeat((data.shape[0],1))
    # there's possibly a way to speed this up:
    # https://stackoverflow.com/questions/27068035/slicing-different-rows-of-a-numpy-array-differently
    batch_states = torch.zeros(
        (batch_size, *data.shape[1:-1], batch_timesteps),
        device=device, dtype=dtype)
    batch_t_eval = torch.zeros(
        (batch_size, batch_timesteps),
        device=device, dtype=dtype)
    for batch_idx in range(batch_size):
        s_idx = sample_indices[batch_idx]
        t_idx = start_time_indices[batch_idx]
        batch_states[batch_idx,...] = data[s_idx,...,t_idx:t_idx+batch_timesteps]
        batch_t_eval[batch_idx,...] = eval_times[s_idx,t_idx:t_idx+batch_timesteps]
    return batch_t_eval, batch_states


def create_inout_sequence(
    data, eval_times,
    train_window=10,
    device=device,
    dtype=torch.double
):
    """Transforms data of shape `(samples, *state_dimensions, time)` by 
    concatenating the previous `train_window` observations along the 
    dimension indexed 1 to conform with torch.nn.RNN inputs i.e. output shape
    is `(samples, train_window, *state_dimensions, time)`.
    
    Time increases along the new dimension.
    """
    data = torch.tensor(data, device=device, dtype=dtype)
    eval_times = torch.tensor(eval_times, device=device, dtype=dtype)
    
    num_samples = data.shape[0]
    state_dimensions = data.shape[1:-1]
    total_time = data.shape[-1]
    # to make the code simpler:
    if len(eval_times.shape) == 1:
        eval_times = eval_times.repeat(num_samples, 1)
    
    return_data = torch.empty(
        num_samples, train_window, *state_dimensions, total_time-train_window,
        device=device, dtype=dtype)
    return_eval_times = torch.empty(
        num_samples, train_window, total_time-train_window,
        device=device, dtype=dtype)
    # count from 1 to total number of windows, then subtract
    for delay in range(train_window):
        return_data[:, delay, ...] = data[..., delay:-train_window+delay]
        return_eval_times[:, delay, :] = eval_times[:, delay:-train_window+delay]
    return return_data, return_eval_times
        
    

##endregion


##region Callbacks
class Callback:
    """Base class for callbacks.
    
    Update self.stop_training to True to cause the training 
    to stop early.
    """
    def __init__(self):
        self.stop_training = False
    
    def __call__(self, losses, validation_losses):
        return self.call(losses, validation_losses)
    
    def call(self, losses, validation_losses):
        pass

    
class JupyterLossPlotCallback(Callback):
    """A Callback to clear and plot losses every `plot_every_n` training steps.
    
    Uses `IPython.display.clear_output` to clear the plot.
    
    Tensorboard is probably a better idea but whatever.
    """
    def __init__(self, plot_every_n, mean_loss=None):
        super(JupyterLossPlotCallback, self).__init__()
        
        import matplotlib.pyplot as plt
        from IPython.display import clear_output
        
        self.plt = plt
        self.clear_output = clear_output
        self.plot_every_n = plot_every_n
        self.mean_loss = mean_loss
        
        
    def call(self, losses, validation_losses):
        if len(losses) % self.plot_every_n == 0:
            self.clear_output(wait=True)
            self.plt.plot(losses, label='training')
            if validation_losses:
                self.plt.plot(validation_losses, label='validation')
            if self.mean_loss is not None:
                plt.avhline(self.mean_loss, 'k--',
                            label='mean loss')
            self.plt.legend(title='loss')
            self.plt.title('training losses')
            self.plt.xlabel('training step')
            self.plt.ylabel('loss')
            self.plt.show()


class EarlyStoppingCallback(Callback):
    """A Callback for early stopping.
    
    It keeps a version of the model parameters which were optimal.
    """
    def __init__(
        self,
        model_reference: torch.nn.Module,
        steps_before_breaking: int,
        load_best_on_breaking: bool=True
    ):
        super(EarlyStoppingCallback, self).__init__()
        
        self.model_reference = model_reference
        self.model_state_dict = self.model_reference.state_dict().copy()
        
        self.steps_before_breaking = steps_before_breaking
        self.lowest_validation_loss = torch.inf
        self.steps_since_lowest = 0
        
        self.load_best_on_breaking = load_best_on_breaking
        
    
    def call(self, losses, validation_losses):
        if validation_losses[-1] <= self.lowest_validation_loss:
            self.steps_since_lowest = 0
            self.lowest_validation_loss = validation_losses[-1]
            self.model_state_dict = self.model_reference.state_dict().copy()
        else:
            self.steps_since_lowest += 1
            if self.steps_since_lowest >= self.steps_before_breaking:
                self.stop_training = True
                if self.load_best_on_breaking:
                    self.model_reference.load_state_dict(
                        self.model_state_dict)


class SaveModelCallback(Callback):
    """A Callback for saving the model."""
    def __init__(
        self,
        model_reference: torch.nn.Module,
        save_fname_prefix: str,
        save_every_n: int=50        
    ):
        super(SaveModelCallback, self).__init__()     
        self.model_reference = model_reference
        self.save_fname_prefix = save_fname_prefix
        self.save_every_n = save_every_n
        
    def call(self, losses, validation_losses):
        if len(losses) % self.save_every_n == 0:
            savepath = '_'.join([
                self.save_fname_prefix,
                f'iter{len(losses)}.pt'])
            torch.save(self.model_reference.state_dict(), savepath)
            
                    
class PlottingCallback(Callback):
    """A Callback for regular plotting.
    
    It can use the model_reference for plotting dynamics if necessary.
    """
    def __init__(
        self,
        model_reference: torch.nn.Module,
        plot_function: Callable[[torch.nn.Module,list,list], Any],
        plot_every_n: int=50        
    ):
        super(PlottingCallback, self).__init__()
        self.model_reference = model_reference
        self.plot_function = plot_function
        self.plot_every_n = plot_every_n
    
    def call(self, losses, validation_losses):
        if len(losses) % self.plot_every_n == 0:
            self.plot_function(
                self.model_reference,
                losses,
                validation_losses)
##endregion


##region Training

# TODO: torch might have this implemented somewhere
def _cycle_dimensions(tensor: torch.Tensor, cycle_by: int=1):
    """Helper function to change timestep dimension after odeint."""
    permutation = *torch.arange(cycle_by, len(tensor.shape)), *torch.arange(cycle_by)
    return tensor.permute(*permutation)
    

# TODO: test for case: fixed_timestep_autonomous=False
def _integrate_batch(
    model, batch_y, batch_t,
    use_adjoint: bool=True,
    fixed_timestep_autonomous: bool=True
):
    """Helper function to integrate the neuralode."""
    if use_adjoint:
        from torchdiffeq import odeint_adjoint as odeint
    else:
        from torchdiffeq import odeint
        
    if fixed_timestep_autonomous:
        integration = odeint(
            model,
            batch_y[..., 0],  # initial values of batch_y
            batch_t[0, :]  # autonomous
        )
        # print(integration.shape)
        # integration = integration.permute(1,2,0)  # match the shape with batch_y
        integration = _cycle_dimensions(integration, cycle_by=1)
    else:
        integration = torch.zeros_like(
            batch_y,
            requires_grad=True,
            device=batch_y.device)
        for batch_sample in range(batch.shape[0]):
            integration[batch_sample, :, :] = odeint(
                model,
                batch_y[batch_sample,...,0],
                batch_t[batch_sample, :]
            ) #.permute(1,2,0)
            integration = _cycle_dimensions(integration, cycle_by=1)
    return integration


def train_model(
    model,
    optimizer,
    training_data,
    training_t_eval,
    validation_data=None,
    validation_t_eval=None,
    training_steps: int=100,
    batch_size: int=16,
    batch_timesteps: int=10,
    callbacks: Optional[Sequence[Callback]]=None,
    losses: Optional[list]=None,
    validation_losses: Optional[list]=None,
    tqdm: Callable=lambda x: x,
    use_adjoint: bool=True,
    fixed_timestep_autonomous: bool=True,
    loss_func: Callable[torch.Tensor,torch.Tensor]=mse_loss
):
    """
    Trains the neuralode `model` using the `optimizer` provided.
    
    `model`: the neuralode to train.
    `optimizer`: the torch optimizer to train the model.
    `training_data`, `validation_data`: torch.Tensors of size shape
        (num_samples, *dimension_of_system, num_timesteps).
    `training_t_eval`, `validation_t_eval`: torch.Tensors of shape
        (num_timesteps,) or (num_samples, num_timesteps).
    `training_steps`: number of training steps to run.
    `batch_size`: batch dimension.
    `batch_timesteps`: length in time of subsamples.
    `callbacks`: instances of Callback to run after each loss evaluation.
    `losses`, `validation_losses`: initial lists of losses and validation
        losses, useful for callbacks.
    `tqdm`: a function, like tqdm.tqdm, that wraps around
        `range(training_steps)`.
    `use_adjoint`: whether to use adjoint method to backpropagate through
        integration of the ODE.
    `fixed_timestep_autonomous`: effectively, whether timesteps can be 
        ignored. This is the case if the system is autonomous and timesteps
        are a fixed length.
    `loss_func`: the loss function, defaults to MSE loss.
    """    
    if losses is None:
        losses = []
    if validation_losses is None:
        validation_losses = []
    
    # prepare model for training
    model.train()
    for trainstep in tqdm(range(training_steps)):
        optimizer.zero_grad()
        # get batch
        batch_t, batch_y = get_batch(
            training_data, training_t_eval,
            batch_size=batch_size,
            batch_timesteps=batch_timesteps)
        # integrate the neuralode
        integration = _integrate_batch(
            model=model,
            batch_y=batch_y, batch_t=batch_t,
            use_adjoint=use_adjoint,
            fixed_timestep_autonomous=fixed_timestep_autonomous)
        # compute loss
        loss = loss_func(integration, batch_y)
        losses.append(loss.item())
        loss.backward()
        
        # do same with validation data
        if validation_data is not None:
            with torch.no_grad():
                val_batch_t, val_batch_y = get_batch(
                    validation_data, validation_t_eval,
                    batch_size=batch_size,
                    batch_timesteps=batch_timesteps)
                val_integration = _integrate_batch(
                    model=model,
                    batch_y=val_batch_y, batch_t=val_batch_t,
                    use_adjoint=use_adjoint,
                    fixed_timestep_autonomous=fixed_timestep_autonomous)
                val_loss = loss_func(val_integration, val_batch_y)
                validation_losses.append(val_loss.item())
        
        # run Callbacks and break if necessary
        if callbacks is not None:
            for callback in callbacks:
                callback(losses, validation_losses)
            stop_training = max(c.stop_training for c in callbacks)
            if stop_training:
                break
        
        # update
        optimizer.step()
    return model, optimizer, losses, validation_losses
##endregion
