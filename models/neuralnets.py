# The neural networks used in fitting neural differential equations
import torch
from torch import nn
from typing import Sequence, Optional

device = 'cuda' if torch.cuda.is_available() else 'cpu'


def create_mlp(layer_sizes, activation=nn.Tanh, device=device):
    layers = []
    for l1,l2 in zip(
        layer_sizes[:-1], layer_sizes[1:]
    ):
        layers.append(nn.Linear(
            l1,l2,
            dtype=torch.double,
            device=device
        ))
        layers.append(activation())
    # drop final activation
    return nn.Sequential(*layers[:-1]).to(device)


class MLP_NeuralODE(nn.Module):
    """A basic neuralode using a multilayer perceptron to compute the derivative.
    
    `state_dimensions`: The dimensions of the state (state does not need to be 
        a vector). For example, it could be `(4,n)` for `n` interacting 
        individuals in an otherwise 4-dimensional space.
    `hidden_layers`: The number of neurons in each hidden layer.
    `activation`: The activation function applied after every linear layer.
    `device`: 'cpu' or 'cuda', where to store the network and its layers.
    """
    def __init__(
        self, 
        state_dimensions: Sequence[int],
        hidden_layers: Sequence[int],
        activation=nn.Tanh,
        device=device
    ):
        super(MLP_NeuralODE, self).__init__()
        self.state_dimensions = torch.tensor(
            state_dimensions, device=device).reshape(-1)
        self.state_dimensions_unraveled = torch.prod(
            self.state_dimensions)
        layer_sizes = [self.state_dimensions_unraveled]\
                        + list(hidden_layers)\
                        + [self.state_dimensions_unraveled]
        self.net = create_mlp(
            layer_sizes=layer_sizes,
            activation=activation,
            device=device)
    
    def forward(self, t, y):
        y_unraveled = y.reshape(-1, self.state_dimensions_unraveled)
        res = self.net(y_unraveled)
        return res.reshape(-1, *self.state_dimensions)
    
    
class Pairwise_NeuralODE(nn.Module):
    """A neuralode which models the dynamics as consisting of pairwise
        interactions (or forces) between individuals, and focal forces
        dependent only on focal individuals i.e.
            $$dy_i/dt = f(y_i) + \sum_j g(y_i, y_j)$$
        $f$ is the focal force, and $g$ is the pairwise force.
    For full generality, a final embedding->output function should be considered.
    
    `state_dimensions`: The dimensions of the state. The state is assumed to 
        have shape `(*spatial_dimensions, number_of_individuals)` so that 
        inputs to the network have shape
        `(batch_size, *spatial_dimensions, number_of_individuals)`
    `hidden_layers`: The number of neurons in each hidden layer. This overwritten
        by `focal_layers` for the focal network, and `pairwise_layers` for the
        pairwise network.
    `activation`: The activation function applied after every linear layer.
    `device`: 'cpu' or 'cuda', where to store the network and its layers.
    `activation`: The activation function applied after every linear layer.
    `device`: 'cpu' or 'cuda', where to store the network and its layers.
    """
    def __init__(
        self, 
        state_dimensions: Sequence[int],
        hidden_layers: Optional[Sequence[int]]=None,
        pairwise_layers: Optional[Sequence[int]]=None,
        focal_layers: Optional[Sequence[int]]=None,
        activation=nn.Tanh,
        device=device
    ):
        super(Pairwise_NeuralODE, self).__init__()
        assert len(state_dimensions) > 1,\
            """Please separate the dimension of individuals from the dimensions
            of the space the individuals are in.
            """
        if pairwise_layers is None:
            pairwise_layers = hidden_layers
        if focal_layers is None:
            focal_layers = hidden_layers
        if pairwise_layers is None or focal_layers is None:
            raise Exception("Focal or pairwise network sizes not specified.")
        self.state_dimensions = torch.tensor(
            state_dimensions, device=device).reshape(-1)
        # We can call the state dimensions which aren't those of 
        # the individuals "spatial".
        self.spatial_dimensions = self.state_dimensions[:-1]
        self.spatial_dimensions_unraveled = torch.prod(
            self.spatial_dimensions)
        
        # pairwise network takes 2 pairs of spatial coordinates
        pairwise_sizes = [2*self.spatial_dimensions_unraveled]\
                        + list(pairwise_layers)\
                        + [self.spatial_dimensions_unraveled]
        self.pairwise_net = create_mlp(
            layer_sizes=pairwise_sizes,
            activation=activation,
            device=device)
        # focal network focuses on only one individual
        focal_sizes = [self.spatial_dimensions_unraveled]\
                        + list(focal_layers)\
                        + [self.spatial_dimensions_unraveled]
        self.focal_net = create_mlp(
            layer_sizes=focal_sizes,
            activation=activation,
            device=device)
    
    
    def forward(self, t, y):
        num_individuals = y.shape[-1]
        derivatives_vector = torch.empty_like(y)
        # this should be potentially sped up with vectorisation
        for focal_idx in range(num_individuals):
            focal_state = y[...,focal_idx]
            derivatives_vector[...,focal_idx] = self.focal_net(
                focal_state)
            for neighbour_idx in range(num_individuals):
                if neighbour_idx == focal_idx: continue
                neighbour_state = y[...,neighbour_idx]
                pairwise_input = torch.cat(
                    [focal_state,neighbour_state],
                    dim=-1)
                derivatives_vector[...,focal_idx] += self.pairwise_net(
                    pairwise_input)
        return derivatives_vector
        