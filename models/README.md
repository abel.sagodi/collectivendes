# models

These python files are to help in fitting neural differential equations using [pytorch](https://pytorch.org/), [torchdiffeq](https://github.com/rtqichen/torchdiffeq) and [torchsde](https://github.com/google-research/torchsde).

## conventions

The data is assumed to have the shape `(sampled_trajectories, *dimensionality_of_system, timepoints)`.